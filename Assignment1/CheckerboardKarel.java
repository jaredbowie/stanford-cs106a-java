/*
 * File: CheckerboardKarel.java
 * ----------------------------
 * When you finish writing it, the CheckerboardKarel class should draw
 * a checkerboard using beepers, as described in Assignment 1.  You
 * should make sure that your program works for all of the sample
 * worlds supplied in the starter folder.
 */

import stanford.karel.*;

public class CheckerboardKarel extends SuperKarel {
	
	public void run(){
		placeBeepersOnOneRow();
		turnTheCorner();
		while (frontIsClear()){
			placeBeepersOnOneRow();
			turnTheCorner();
		}
	}
	
	/*
	 * Precondition: facing towards the open row.
	 * Place a beeper on every other square until the row ends.
	 */
	
	private void placeBeepersOnOneRow(){
		while (frontIsClear()){	
			putBeeper();
			move();
			if (frontIsClear()){
				move();
			}
		}
	}
	
	/*
	 * After row is finished turn the corner and face towards the next empty row.
	 */
	
	private void turnTheCorner(){
		if(facingWest()){
			turnRight();
			if(frontIsClear()){
				move();
				turnRight();
			}
		}
		else {
			turnLeft();
			if(frontIsClear()){
				move();
				turnLeft();
			}
		}
	}
}
		
		
