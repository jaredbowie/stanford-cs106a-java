/*
 * File: MidpointFindingKarel.java
 * -------------------------------
 * When you finish writing it, the MidpointFindingKarel class should
 * leave a beeper on the corner closest to the center of 1st Street
 * (or either of the two central corners if 1st Street has an even
 * number of corners).  Karel can put down additional beepers as it
 * looks for the midpoint, but must pick them up again before it
 * stops.  The world may be of any size, but you are allowed to
 * assume that it is at least as tall as it is wide.
 */

import stanford.karel.*;

public class MidpointFindingKarel extends SuperKarel {
	public void run(){
		if (frontIsClear()){
			putBeeper();
			move();
			while(noBeepersPresent()){
				putBeeperOnOtherSide();
			}
			turnAround();
			move();
			putBeeper(); // midpoint now has 2
			cleanUpBeepers();
		}
		else {
			putBeeper();
		}
	}
	
	private void putBeeperOnOtherSide(){
		goToEndPutBeeper();
	}
	
	/*
	 * Put a beeper on the other end one in from the last beeper 
	 * or the wall if no beepers
	 */
	
	private void goToEndPutBeeper(){
		while(frontIsClear() && noBeepersPresent()){
			move();
		}
		if(beepersPresent()){
			turnAround();
			move();
			putBeeper();
		}
		else {
			turnAround();
			putBeeper();
		}
		move();
	}
	
	/*
	 * Remove one beeper from every spot of a row.
	 * Precondition: Can be starting anywhere, must be facing east or west
	 */
	
	private void cleanUpBeepers(){
		goToBeginningOfRow();
		turnAround();
		takeOneBeeperOffEverySpot();
	}
	
	
	private void goToBeginningOfRow(){
		while(frontIsClear()){
			move();
		}
	}

	
	/*
	 * Remove one beeper from every spot in a row
	 */
	private void takeOneBeeperOffEverySpot(){
		while(frontIsClear()){
			pickBeeper();
			move();
		}
		pickBeeper();
	}
}