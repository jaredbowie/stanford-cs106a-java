/*
 * File: CollectNewspaperKarel.java
 * --------------------------------
 * At present, the CollectNewspaperKarel subclass does nothing.
 * Your job in the assignment is to add the necessary code to
 * instruct Karel to walk to the door of its house, pick up the
 * newspaper (represented by a beeper, of course), and then return
 * to its initial position in the upper left corner of the house.
 */

import stanford.karel.*;

public class CollectNewspaperKarel extends SuperKarel {
	public void run() {
		getBeeperOutside();
		returnToInitialPosition();
		} 
	
	/* 
	 * Assuming there is a beeper outside and the house is as layed out originally.
	 * Go outside pick up a beeper. 
	 */
	private void getBeeperOutside() {
		move();
		move();
		turnRight();
		move();
		turnLeft();
		move();
		pickBeeper();
	}
	
	/*
	 * Return to house and the original position started from.
	 */
	
	private void returnToInitialPosition() {
		turnAround();
		move();
		turnRight();
		move();
		turnLeft();
		move();
		move();
		turnAround();
	}
	
}
