/*
 * File: StoneMasonKarel.java
 * --------------------------
 * The StoneMasonKarel subclass as it appears here does nothing.
 * When you finish writing it, it should solve the "repair the quad"
 * problem from Assignment 1.  In addition to editing the program,
 * you should be sure to edit this comment so that it no longer
 * indicates that the program does nothing.
 */

import stanford.karel.*;

public class StoneMasonKarel extends SuperKarel {
	public void run()	{
		while (frontIsClear()){
			pickUpAllBeepersOneRow();
			moveOverFourSpaces();
		}
		pickUpAllBeepersOneRow();
	}
	
	/*
	 * Precondition facing east.
	 */
	
	private void pickUpAllBeepersOneRow(){
		moveToBottom();
		pickBeepersWhileMovingToTop();
	}
	
	/*
	 * Move to bottom of column because we don't know how tall a column is.
	 * Precondition: Facing east
	 */
	
	private void moveToBottom(){
		turnRight();
		goToBottom();
	}
	
	/*
	 * Go to bottom of column
	 * Precondition: Facing south.
	 */
	
	private void goToBottom(){
		while(frontIsClear()){
			move();
		}
	}
	
	/* 
	 * Precondition: Facing Right
	 */
	
	private void moveOverFourSpaces() {
		move();
		move();
		move();
		move();
	}
	
	/*
	 * Precondition: facing south
	 */
	
	private void pickBeepersWhileMovingToTop(){
		turnAround();
		while(frontIsClear()){
			if(beepersPresent()){
				pickBeeper();
			}
			move();
		}
		if(beepersPresent()){
			pickBeeper();
		}
		turnRight();
	}
	
	
	


	
	
}
